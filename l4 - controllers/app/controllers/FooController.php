<?php

class FooController extends Controller {

	/*public function __construct(){
		$this->beforeFilter(function(){
			return 'filter callback';
		});
	}*/
	
	
	
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
		return 'foo index';
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
		return 'showing foo index '.$id;
	}

}