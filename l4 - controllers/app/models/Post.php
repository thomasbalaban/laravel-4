<?php

	class Post extends Eloquent{
		protected $table = 'posts';
		
		public function comment(){
			return $this -> hasMany('Comment', 'posts-id');	
		}
	}