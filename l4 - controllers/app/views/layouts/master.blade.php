<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>{{ $title }}</title>
{{ HTML::style('css/style.css') }}
</head>

<body>
	@include('layouts.nav')
	@yield('content')
</body>
</html>