@extends('layouts.master')
@section('content')

<div class="welcome">
	<h1>This is a practice page</h1>
</div>

{{ Form::open(array('url' => '/')) }}
{{ Form::text('fname', '', array('placeholder' => 'First Name')) }}
{{ Form::text('lname', '', array('placeholder' => 'Last Name')) }}
{{ Form::submit('Add Name', array('class' => 'btn btn-success')) }}
{{ Form::close() }}

@stop