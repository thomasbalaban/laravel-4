<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

/*Route::get('/', function()
{
	return View::make('hello');
});
*/


Route::get('/', function()
{
	$title = "Test L4";
	return View::make('home.index')
		->with('title', $title);
		
	$user = DB::select('select * from employees where Country =?', array('United Kingdom'));
	
	return $user;
	
	$input = Input::all();
	
	DB::insert('insert into test (fname, lname) values (?, ?)', array($input['fname'], $input['lname']));
	
	dd($user);
});

Route::get('about', function(){
	$title = 'about';
	return View::make('home.about')
		->with('title', $title);
});

//Route::get('mycontroller', array('uses' => 'MyController@loadView'));

	/*$title = "Test L4";
	return View::make('home.index')
		->with('title', $title);*/
